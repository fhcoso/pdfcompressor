# pdfcompressor
script using ghostscript to compress pdf file

## Flags

### Quality

    default : -2

    --screen or -0
    --ebook or -1
    --print or -2
    --prepress or -3

### Compatibility Level

    --compatibility or -c (default : 1.4)

### File

    --output or -o (default : compressed.pdf)

    --input or -i

## Example

    ./pdfcompressor.sh -2 -i latex-output.pdf
